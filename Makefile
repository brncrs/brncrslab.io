PUBLICPATH=public

MAKEINFO = makeinfo

MAINSRC = src/main.texi
SUBSRC = src/fdl-1.3.texi
SOURCES = $(MAINSRC) $(SUBSRC)

html: html-timestamp

html-timestamp: $(SOURCES)
	$(MAKEINFO) --html -o $(PUBLICPATH)/ $(MAINSRC)
	touch $@

	
